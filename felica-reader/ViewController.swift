//
//  ViewController.swift
//  felica-reader
//
//  Created by treastrain on 2019/06/06.
//  Copyright © 2019 treastrain / Tanaka Ryoga. All rights reserved.
//

import UIKit
import CoreNFC

class ViewController: UIViewController, NFCTagReaderSessionDelegate {

    var session: NFCTagReaderSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func beginScanning(_ sender: UIButton) {
        guard NFCTagReaderSession.readingAvailable else {
            let alertController = UIAlertController(
                title: "Scanning Not Supported",
                message: "This device doesn't support tag scanning.",
                preferredStyle: .alert
            )
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        print("masuk")
        self.session = NFCTagReaderSession(pollingOption: [.iso14443,.iso15693], delegate: self)
        self.session?.alertMessage = "Letakkan kartu e-money mandiri anda pada bagian atas ponsel"
        self.session?.begin()
    }
    
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("aktif")
        print("tagReaderSessionDidBecomeActive(_:)")
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        if let readerError = error as? NFCReaderError {
            if (readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead)
                && (readerError.code != .readerSessionInvalidationErrorUserCanceled) {
                let alertController = UIAlertController(
                    title: "Session Invalidated",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        self.session = nil
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        
        if tags.count > 1 {
            let retryInterval = DispatchTimeInterval.milliseconds(500)
            session.alertMessage = "More than 1 tag is detected, please remove all tags and try again."
            DispatchQueue.global().asyncAfter(deadline: .now() + retryInterval, execute: {
                session.restartPolling()
            })
            return
        }
        
        let tag = tags.first!
        
        session.connect(to: tag) { (error) in
            print("session connect")
            if nil != error {
                session.invalidate(errorMessage: "Connection error. Please try again.")
                return
            }
        
//            guard case .feliCa(let feliCaTag) = tag else {
//                let retryInterval = DispatchTimeInterval.milliseconds(500)
//                session.alertMessage = "A tag that is not FeliCa is detected, please try again with tag FeliCa."
//                DispatchQueue.global().asyncAfter(deadline: .now() + retryInterval, execute: {
//                    session.restartPolling()
//                })
//                return
//            }
            
            guard case .iso7816(let emoneyTag) = tag else {
                let retryInterval = DispatchTimeInterval.milliseconds(500)
                session.alertMessage = "emoney bukan? loe cek dlo lah"
                DispatchQueue.global().asyncAfter(deadline: .now() + retryInterval, execute: {
                    session.restartPolling()
                })
                return
            }
            
            let UID = emoneyTag.identifier.hexEncodedString()
            let ATS = emoneyTag.historicalBytes!
            let AID = emoneyTag.initialSelectedAID
            
            print("UID : \(UID)")
            print("ATS : \(ATS)")
            print("AID : \(AID)")
            
            let commandAPDUForPrePaidData = Data(bytes: [0, 0xA4, 0x04, 0, 0x08, 0, 0, 0, 0, 0, 0, 0, 0x01])
            let commandAPDUForNumberData = Data(bytes: [0, 0xB3, 0, 0, 0x3F])
            let commandAPDUForBalanceData = Data(bytes: [0, 0xB5, 0, 0, 0x0A])
            let commandAPDUForCertificateData = Data(bytes: [0, 0xE0, 0, 0, 0])
            let commandAPDUForATRData = Data(bytes: [0, 0xF2, 0x10, 0, 0x0B])
            let commandAPDUForReversalData = Data(bytes: [0, 0xE7, 0, 0])
            
            let commandAPDU = NFCISO7816APDU.init(data: commandAPDUForNumberData)!
            let commandAPDU2 = NFCISO7816APDU.init(data: commandAPDUForReversalData)!
            
            emoneyTag.sendCommand(apdu: commandAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?)
                in
                    var message = ""
                    
                    print(Int8(bitPattern: sw1))
                
                    if error != nil {
                        session.invalidate(errorMessage: error!.localizedDescription)
                        return
                    }
                    
                    // get prepaid
//                    let i = response.count
//
//                if sw1 == 144 {
//                    emoneyTag.sendCommand(apdu: commandAPDU2) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?)
//                        in
//                        print(response)
//                    }
//                } else {
//                    session.alertMessage = message
//                    session.invalidate()
//                }

//                    if Int8(bitPattern: response[i - 2]) != -112 || Int8(bitPattern: response[i - 1]) != 0
//                    {
//                        session.invalidate(errorMessage: "nfcError_CARDUNKNOWN")
//                        return
//                    } else {
//                        message = "Prepaid Ok"
//                    }
                    
                    // get balance
//                    var balance:Int = 0
//                    var il = 0
//                    while(il<4) {
//                        let j1 = Int8(bitPattern: response[il])
//                        let xx = (255 & Int(j1)) << (il * 8)
//                        balance += xx
//                        il = il + 1
//                    }
//                    message = String.init(format: "Saldo Loe %@", String(balance))
                    
                    // get card no
                    let cardNo = response.hexEncodedString()
                
                message = cardNo[0..<4] + "-" + cardNo[4..<8] + "-" + cardNo[8..<12] + "-" + cardNo[12..<16]
                print(message)
                
//                message = cardNo.substring(with: 0 ..< 4) + "-" + cardNo.substring(with: 4..<8) + "-" + cardNo.substring(with: 8..<12) + "-" + cardNo.substring(with: 12..<16)
                
//                let cardNo = response.hexEncodedString()
//                print(cardNo)
                  
//                let cardATR = response.hexEncodedString()
//                    message = cardATR
//                print(message)
//
                    session.alertMessage = message
                    session.invalidate()
                
            }
            
//            session.alertMessage = "Sukses Cuk!"
            
//            let idm = (emoneyTag.currentIDm.map { String(format: "%.2hhx", $0) } as AnyObject).joined()
//            let systemCode = (emoneyTag.currentSystemCode.map { String(format: "%.2hhx", $0) } as AnyObject).joined()
//
//            print("IDm: \(idm)")
//            print("System Code: \(systemCode)")
            
//            session.alertMessage = "Read success!\nIDm: \(idm)\nSystem Code: \(systemCode)"
//            session.invalidate()
        }
    }

}

extension Data {
    private static let hexAlphabet = "0123456789abcdef".unicodeScalars.map { $0 }

    public func hexEncodedString() -> String {
        return String(self.reduce(into: "".unicodeScalars, { (result, value) in
            result.append(Data.hexAlphabet[Int(value/16)])
            result.append(Data.hexAlphabet[Int(value%16)])
        }))
    }
}

extension String {

  var length: Int {
    return count
  }

  subscript (i: Int) -> String {
    return self[i ..< i + 1]
  }

  func substring(fromIndex: Int) -> String {
    return self[min(fromIndex, length) ..< length]
  }

  func substring(toIndex: Int) -> String {
    return self[0 ..< max(0, toIndex)]
  }

  subscript (r: Range<Int>) -> String {
    let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                        upper: min(length, max(0, r.upperBound))))
    let start = index(startIndex, offsetBy: range.lowerBound)
    let end = index(start, offsetBy: range.upperBound - range.lowerBound)
    return String(self[start ..< end])
  }

}

//extension Data {
//    struct HexEncodingOptions: OptionSet {
//        let rawValue: Int
//        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
//    }
//
//    func hexEncodedString(options: HexEncodingOptions = []) -> String {
//        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
//        return map { String(format: format, $0) }.joined()
//    }
//    func hexEncodedString() -> String {
//      let hexDigits = Array("0123456789abcdef".utf16)
//      var hexChars = [UTF16.CodeUnit]()
//        hexChars.reserveCapacity(self.count * 2)
//
//      for byte in self {
//        let (index1, index2) = Int(byte).quotientAndRemainder(dividingBy: 16)
//        hexChars.append(hexDigits[index1])
//        hexChars.append(hexDigits[index2])
//      }
//
//      return String(utf16CodeUnits: hexChars, count: hexChars.count)
//    }
//}
